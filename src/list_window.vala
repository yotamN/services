/* window.vala
 *
 * Copyright 2021 Yotam Nachum
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Services {
	[GtkTemplate (ui = "/net/yotam/systemd/Services/list_window.ui")]
	public class ListWindow : Adw.ApplicationWindow {
		[GtkChild]
		unowned Gtk.ToggleButton search_button;

		[GtkChild]
		unowned Gtk.SearchBar search_bar;

		[GtkChild]
		unowned Gtk.SearchEntry search_entry;

		[GtkChild]
		unowned Gtk.ListBox list;

		public ListWindow (Gtk.Application app) {
			Object (application: app);
			this.search_bar.notify.connect(toggle_search_button);
			this.search_bar.connect_entry(this.search_entry);
			this.populate_list.begin();
		}

		private async void populate_list() {
			Manager manager = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", "/org/freedesktop/systemd1");
			var units = yield manager.list_units();
			foreach (var unit in units) {
				if (unit.name.has_suffix(".service")) {
					var row = new Row(unit);
					list.append(row);
				}
			}
		}

		[GtkCallback]
		public void toggle_search_bar() {
			if (this.search_bar.search_mode_enabled != this.search_button.active) {
				this.search_bar.search_mode_enabled = this.search_button.active;
			}
		}

		// [GtkCallback]
		public void toggle_search_button(ParamSpec pspec) {
			if (this.search_bar.search_mode_enabled != this.search_button.active) {
				this.search_button.active = this.search_bar.search_mode_enabled;
			}
		}

		[GtkCallback]
		public void search_changed() {
			this.list.set_filter_func((row) => {
				var unit_row = (Row) row;
				return unit_row.title.down().contains(this.search_entry.text.down());
			});
		}
	}
}
