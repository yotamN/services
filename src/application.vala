namespace Services {
	public class Application : Gtk.Application {
		public Application () {
			Object (application_id: "net.yotam.systemd.Services", flags: ApplicationFlags.FLAGS_NONE);
    }

		public override void startup () {
			base.startup();
			Adw.init();
		}

		public override void activate () {
			base.activate();
			var win = this.active_window;
			if (win == null) {
				win = new Services.ListWindow (this);
			}
			win.present ();
		}
	}
}
