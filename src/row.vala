namespace Services {
	[GtkTemplate (ui = "/net/yotam/systemd/Services/row.ui")]
	class Row : Adw.ExpanderRow {
		private int NAME_SUFFIX_LENGTH = 8;

		[GtkChild]
		unowned Gtk.MenuButton actions_button;

		[GtkChild]
		unowned Gtk.Label status_value_label;

		[GtkChild]
		unowned Gtk.Label definition_file_value_label;

		[GtkChild]
		unowned Gtk.Label main_pid_value_label;

		private UnitMetadata unit_metadata;

    private const ActionEntry[] ACTION_ENTRIES = {
        { "reload_service", on_reload_service },
        { "stop_service", on_stop_service },
        { "start_service", on_start_service },
        { "disable_service", on_disable_service },
        { "enable_service", on_enable_service },
    };

		public Row(UnitMetadata unit) {
			this.unit_metadata = unit;
			this.title = unit.name.slice(0, unit.name.length - this.NAME_SUFFIX_LENGTH);
			if (unit.description.strip() == "") {
				this.subtitle = "No description";
			} else {
				this.subtitle = Markup.escape_text(unit.description);
			}

			var actions = new SimpleActionGroup();
			actions.add_action_entries(ACTION_ENTRIES, this);
			this.insert_action_group("row", actions);
		}

		[GtkCallback]
		public async void on_expanded_changed() {
			if (this.expanded == false) {
				return;
			}

			Unit unit = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);
			Service service = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);

			var file_state = unit.unit_file_state;
			var load_state = unit.load_state;
			var active_state = unit.active_state;
			var sub_state = unit.sub_state;

			this.status_value_label.label = load_state + ", " + active_state + ", " + sub_state;
			this.definition_file_value_label.label = unit.fragment_path;
			this.main_pid_value_label.label = service.main_pid.to_string();

			var menu = new Menu();
			if (unit.can_reload) {
				menu.append("Reload", "row.reload_service");
			}
			if (active_state == "inactive" || active_state == "failed") {
				if (file_state == "masked" || file_state == "masked_runtime") {
					menu.append("Start", "row.start_service_unavailable");
				} else {
					menu.append("Start", "row.start_service");
				}
			} else {
				menu.append("Stop", "row.stop_service");
			}

			if (file_state == "enabled") {
				menu.append("Disable", "row.disable_service");
			} else if (file_state == "disabled") {
				menu.append("Enable", "row.enable_service");
			} else if (file_state == "static") {
				menu.append("Enable", "row.enable_service_unavailable");
			}
			var popover = new Gtk.PopoverMenu.from_model(menu);
			this.actions_button.popover = popover;
		}

		public void on_reload_service() {
			this.reload_service.begin();
		}

		private async void reload_service() {
			Unit unit = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);
			yield unit.reload("replace");
		}

		public void on_stop_service() {
			this.stop_service.begin();
		}

		private async void stop_service() {
			Unit unit = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);
			yield unit.stop("replace");
		}

		public void on_start_service() {
			this.start_service.begin();
		}

		private async void start_service() {
			Unit unit = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);
			yield unit.start("replace");
		}

		public void on_disable_service() {
			this.disable_service.begin();
		}

		private async void disable_service() {
			Manager manager = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", "/org/freedesktop/systemd1");
			Unit unit = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);
			var changes = yield manager.disable_unit_files({ "test_service.service" }, false);
			if (changes.length > 0) {
				yield manager.reload();
			}
		}

		public void on_enable_service() {
			this.enable_service.begin();
		}

		private async void enable_service() {
			Manager manager = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", "/org/freedesktop/systemd1");
			Unit unit = yield Bus.get_proxy(BusType.SESSION, "org.freedesktop.systemd1", this.unit_metadata.object_path);

			bool carries_install_info;
			UnitFileChange[] changes;
			yield manager.enable_unit_files({ "test_service.service" }, false, false, out carries_install_info, out changes);
			if (changes.length > 0) {
				yield manager.reload();
			}
		}
	}
}
