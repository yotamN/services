namespace Services {
	[DBus (name = "org.freedesktop.DBus.Introspectable")]
	interface Introspectable : Object {
		public abstract async string Introspect() throws GLib.Error;
	}

	struct UnitMetadata {
		public string name;
		public string description;
		public string load_state;
		public string active_state;
		public string sub_state;
		public string follower;
		public string object_path;
		public uint job_id;
		public string job_type;
		public string job_object_path;
	}

	struct UnitFileChange {
		public string type;
		public string symlink_name;
		public string symlink_destination;
	}

	[DBus (name = "org.freedesktop.systemd1.Manager")]
	interface Manager : Object {
		[DBus (name = "GetUnit")]
		public abstract async string get_unit(string name) throws GLib.Error;

		[DBus (name = "ListUnits")]
		public abstract async UnitMetadata[] list_units() throws GLib.Error;

		[DBus (name = "Reload")]
		public abstract async void reload() throws GLib.Error;

		[DBus (name = "EnableUnitFiles")]
		public abstract async void enable_unit_files(string[] files, bool runtime, bool force, out bool carries_install_info, out UnitFileChange[] changes) throws GLib.Error;

		[DBus (name = "DisableUnitFiles")]
		public abstract async UnitFileChange[] disable_unit_files(string[] files, bool runtime) throws GLib.Error;
	}

	[DBus (name = "org.freedesktop.systemd1.Unit")]
	interface Unit : Object {
		[DBus (name = "Start")]
		public abstract async string start(string mode) throws GLib.Error;

		[DBus (name = "Stop")]
		public abstract async string stop(string mode) throws GLib.Error;

		[DBus (name = "Reload")]
		public abstract async string reload(string mode) throws GLib.Error;

		[DBus (name = "DropInPaths")]
		public abstract string[] drop_in_paths {  owned get; }

		[DBus (name = "SourcePath")]
		public abstract string source_path {  owned get; }

		[DBus (name = "FragmentPath")]
		public abstract string fragment_path {  owned get; }

		[DBus (name = "UnitFileState")]
		public abstract string unit_file_state {  owned get; }

		[DBus (name = "LoadState")]
		public abstract string load_state {  owned get; }

		[DBus (name = "ActiveState")]
		public abstract string active_state {  owned get; }

		[DBus (name = "SubState")]
		public abstract string sub_state {  owned get; }

		[DBus (name = "CanReload")]
		public abstract bool can_reload {  get; }
	}

	[DBus (name = "org.freedesktop.systemd1.Service")]
	interface Service : Object {
		[DBus (name = "MainPID")]
		public abstract uint main_pid {  get; }
	}
}
